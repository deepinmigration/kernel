KERVER := 4.14.13

all: 

kernel:
	@echo prepare kernel.Please run make menuconfig and make bindeb-pkg
	install -Dm644 config.kernel src/linux-$(KERVER)/.config

extract:
	@rm -rf fakeroot
	KERNELPKG=`ls src | grep linux-image-$(KERVER) | sort -V -r | head -n 1`; \
		  echo "Decompressing $${KERNELPKG} to fakeroot ..."; \
		  dpkg -x src/$${KERNELPKG} fakeroot ; \
		  dpkg -e src/$${KERNELPKG} fakeroot/DEBIAN

broadcom-wl:
	if [ ! -d fakeroot ];then echo "no kernel prepare" && exit 101;fi
	cd src/broadcom-wl/src; make -C ../../linux-${KERVER}/ M=`pwd`
	install -Dm644 src/broadcom-wl/src/wl.ko fakeroot/lib/modules/$(KERVER)/extramodules/wl.ko

bindeb-pkg: 
	depmod -b fakeroot $(KERVER)
	cd fakeroot; find . -type f ! -path './DEBIAN/*' -printf '%P\0' \
		| xargs -r0 md5sum > DEBIAN/md5sums
	fakeroot dpkg -b fakeroot .tmp.deb
	dpkg-name .tmp.deb
